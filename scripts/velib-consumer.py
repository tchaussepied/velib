#!/usr/bin/env python
# coding: utf-8

# In[1]:


# pip install elasticsearch


# In[2]:


# pip install pyspark


# In[3]:


# pip install kafka-python


# In[4]:


import os

os.environ[
    "PYSPARK_SUBMIT_ARGS"
] = "--packages org.apache.spark:spark-sql-kafka-0-10_2.12:3.0.1,org.elasticsearch:elasticsearch-spark-20_2.10:7.10.1 pyspark-shell"


# In[5]:


import json
from _collections import defaultdict
import math

from pyspark.conf import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.types import *

from kafka import KafkaConsumer
from elasticsearch import Elasticsearch
from subprocess import check_output


# In[6]:


topic = "velib-stations"
ETH0_IP = check_output(["hostname"]).decode(encoding="utf-8").strip()

SPARK_MASTER_URL = "local"
SPARK_DRIVER_HOST = ETH0_IP


# In[7]:


spark_conf = SparkConf()
spark_conf.setAll(
    [
        ("spark.master", SPARK_MASTER_URL),
        ("spark.driver.bindAddress", "0.0.0.0"),
        ("spark.driver.host", SPARK_DRIVER_HOST),
        ("spark.app.name", "Velib"),
        ("spark.submit.deployMode", "client"),
        ("spark.ui.showConsoleProgress", "true"),
        ("spark.eventLog.enabled", "false"),
        ("spark.logConf", "false"),
    ]
)


# In[8]:


def getrows(df, rownums=None):
    return df.rdd.zipWithIndex().filter(lambda x: x[1] in rownums).map(lambda x: x[0])


# In[9]:


def writeElasticSearch(df, epoch_id):
    print("Write to ElasticSearch")
    
    i = 0
    
    while i < 10:
    # 2574:
    
        station = getrows(df, rownums=[i]).collect()

        print(station)
    
        if not station:
            return

        es_con = {
            "address": station[0][0],
            "available_bike_stands": station[0][1],
            "available_bikes": station[0][2],
            "banking": station[0][3],
            "bike_stands": station[0][4],
            "bonus": station[0][5],
            "contract_name": station[0][6],
            "last_update":  station[0][7],
            "name": station[0][8],
            "number": station[0][9],
            "position" : {
                "lat" : station[0][10],
                "lng" : station[0][11]
            },
            "status" : station[0][12]
        }
    
        rdd = spark.sparkContext.parallelize([es_con])
        new_rdd = rdd.map(lambda x: ("key", x))
        new_rdd.saveAsNewAPIHadoopFile(
            path="-",
            outputFormatClass="org.elasticsearch.hadoop.mr.EsOutputFormat",
            keyClass="org.apache.hadoop.io.NullWritable",
            valueClass="org.elasticsearch.hadoop.mr.LinkedMapWritable",
            conf={
                "es.nodes": "localhost",
                "es.port": "9200",
                "es.nodes.data.only": "false",
                "es.resource": "velib"
            }
        )
        
        i += 1
        


# In[10]:


def writeElasticSearch2(df, epoch_id):
   
    print("Write to ElasticSearch")
    i=0
    while i<2573:
        try:
            station = getrows(df, rownums=[i]).collect()
        except:
            return
        if not station:
            return
        position = {
            "lat": station[0][10],
            "lon": station[0][11]
        }

        es_con = {
            "address": station[0][0],
            "contract_name": station[0][6],
            "available_bikes": station[0][2],
            "available_bike_stands": station[0][1],
            "date": station[0][7] if(station[0][7]!="N") else None,
            "position" : position if(station[0][7]!="N") else None
        }
        #print(es_con)

        rdd = spark.sparkContext.parallelize([es_con])
        new_rdd = rdd.map(lambda x: ("key", x))
        new_rdd.saveAsNewAPIHadoopFile(
            path="-",
            outputFormatClass="org.elasticsearch.hadoop.mr.EsOutputFormat",
            keyClass="org.apache.hadoop.io.NullWritable",
            valueClass="org.elasticsearch.hadoop.mr.LinkedMapWritable",
            conf={
                "es.nodes": "localhost",
                "es.port": "9200",
                "es.resource": "velib",
                #"es.input.json": "yes",
                #"es.mapping.id": "doc_id",
                #"checkpointLocation": "/home/formation/Bureau/CheckPoint"  
            },
        )
        i+=1
        print(f"{i} : timestamp {station[0][7]}")
     
    print(f"{i}rows inserted")


# In[11]:


spark = SparkSession.builder.config(conf=spark_conf).getOrCreate()
spark.sparkContext.setLogLevel("ERROR")


# In[12]:


spark


# In[13]:


df = (
    spark.readStream.format("kafka")
    .option("kafka.bootstrap.servers", "localhost:9092")
    .option("subscribe", "velib")
    .option("enable.auto.commit", "true")
    .load()
)
# df.dropDuplicates("name")


# In[14]:


detail_df1 = df.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
  
detail_schema = (
     StructType()
    .add("address", StringType(), True)
    .add("available_bike_stands", StringType(), True)
    .add("available_bikes", StringType(), True)
    .add("banking", StringType(), True)
    .add("bike_stands", StringType(), True)
    .add("bonus", StringType(), True)
    .add("contract_name", StringType(), True)
    .add("last_update", StringType(), True)
    .add("name", StringType(), True)
    .add("number", StringType(), True)
    .add("lat", DoubleType(), True)
    .add("lng", DoubleType(), True)
    .add("status", StringType(), True)
)


# In[15]:


detail_df2 = detail_df1.select(from_json(col("value"), detail_schema).alias("velib_detail")).select(
    "velib_detail.address",
    "velib_detail.available_bike_stands",
    "velib_detail.available_bikes",
    "velib_detail.banking",
    "velib_detail.bike_stands",    
    "velib_detail.bonus",
    "velib_detail.contract_name",
    "velib_detail.last_update",
    "velib_detail.name",
    "velib_detail.number",
    "velib_detail.lat",
    "velib_detail.lng",
    "velib_detail.status"
)


# In[ ]:


print("Staring PySpark streaming")

parsed_json = detail_df2.writeStream.foreachBatch(writeElasticSearch2).start()

print("Pyspark : step 2")

parsed_json.awaitTermination()

print("PySpark Structured Streaming with Kafka Application Completed....")


# In[ ]:




