import json
import time
import urllib.request
from kafka import KafkaProducer
API_KEY = "94a5f88d8efb9307d2fa89b4ece05c7e260e6a08"
url = "https://api.jcdecaux.com/vls/v1/stations?apiKey={}".format(API_KEY)
producer = KafkaProducer(bootstrap_servers="localhost:9092")
while True:
	response = urllib.request.urlopen(url)
	stations = json.loads(response.read().decode())
	for station in stations:
		stationenvoi = {
		'number': station['number'], 
		'contract_name': station['contract_name'], 
		'name': station['name'], 
		'address': station['address'],
		'lat': station['position']['lat'],
		'lng': station['position']['lng'],
		'banking': station['banking'], 
		'bonus': station['bonus'], 
		'bike_stands': station['bike_stands'], 
		'available_bike_stands': station['available_bike_stands'], 
		'available_bikes': station['available_bikes'], 
		'status': station['status'], 
		'last_update': str(station["last_update"])[:-3]}
		producer.send("velib", json.dumps(stationenvoi).encode())
	print("{} Produced {} station records".format(time.time(), len(stations)))
	time.sleep(1)

